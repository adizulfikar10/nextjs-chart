const path = require('path')
const headers = require('./headers')

module.exports = {
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  // webpack5: true,
  // async headers() {
  //   return [
  //     {
  //       source: "/(.*)",
  //       headers
  //     }
  //   ];
  // }
}
