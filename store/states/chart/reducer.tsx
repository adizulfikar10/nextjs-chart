import { IChart } from './state'
import * as alertActions from './actions'

const initialState: IChart = {
  data: [],
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case alertActions.SET_CHART:
      return Object.assign({}, state, {
        data: action.data.data,
      })
    case alertActions.RESET_CHART:
      return Object.assign({}, state, initialState)
    default:
      return state
  }
}

export default reducer
