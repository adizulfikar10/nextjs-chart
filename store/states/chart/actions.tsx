import { getChart } from '../../../api/chart/chart'
import { IChart } from './state'

export const SET_CHART = 'SET_CHART'
export const RESET_CHART = 'RESET_CHART'

export const fetchChart = (payload: IChart) => async (dispatch: any) => {
  try {
    const response = await getChart(payload)
    const data = response.data
    return dispatch({ type: SET_CHART, data })
  } catch (error) {
    console.log(error)
  }
}

export const resetChart = () => (dispatch) => {
  return dispatch({ type: RESET_CHART })
}
