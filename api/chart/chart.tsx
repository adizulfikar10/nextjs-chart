import { requestMain } from "../../api";
import generateQueryString from "../../api/common/queryGenerator";

export const getChart = (payload: any) => {
  const params = generateQueryString(payload.params);
  return requestMain({
    url: `chart-consumer/lightweight/history?${params}`,
    method: "get",
    headers: {
      Authorization: "Bearer " + payload.token
    }
  });
};
