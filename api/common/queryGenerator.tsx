const generateQueryString = (params) => {
  const queries = []
  if (params.code) {
    queries.push(`code=${params.code}`)
  }
  if (params.from) {
    queries.push(`from=${params.from}`)
  }
  if (params.to) {
    queries.push(`to=${params.to}`)
  }
  if (params.resolution) {
    queries.push(`resolution=${params.resolution}`)
  }
  if (params.range) {
    queries.push(`range=${params.range}`)
  }

  queries.push('cache=0')
  const queryString = queries.join('&')
  return queryString
}

export default generateQueryString
