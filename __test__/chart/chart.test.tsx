import { render } from '@testing-library/react'
import Index from '../../pages/index'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import renderer from 'react-test-renderer'


describe('Analysis Component', () => {
  const initialState = {}
  const mockStore = configureStore()
  let store

  it('renders correctly', () => {
    store = mockStore(initialState)
    const tree = renderer
      .create(
        <Provider store={store}>
          <Index></Index>
        </Provider>
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
