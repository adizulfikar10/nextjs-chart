import dynamic from 'next/dynamic'
import Image from 'next/image'
import { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchChart } from '../store/states/chart/actions'

interface IProps {
  fetchChart: any
  chart: any
}

const Index = (props: IProps) => {
  const ChartCandle = dynamic(() => import('@/components/chartCandle'), {
    ssr: false,
  })

  const { fetchChart, chart } = props

  const [loading, setLoading] = useState(false)
  const [token, setToken] = useState('')
  const [params, setParams] = useState({
    range: '1M',
    from: '',
    to: '',
    resolution: '',
    code: '',
  })
  const [chartData, setChartData] = useState([])

  const rangeList = ['1M', '3M', 'YTD', '1Y', '3Y', '5Y']

  useEffect(() => {
    function handleResize() {
      setLoading(true)
      setTimeout(() => {
        setLoading(false)
      }, 500)
    }

    window.addEventListener('resize', handleResize)
  })

  const parseHttpHeaders = (httpHeaders: string) => {
    return httpHeaders
      .split('\r\n')
      .map((x) => x.split(/: */, 2))
      .filter((x) => x[0])
      .reduce((ac, x) => {
        ac[x[0]] = x[1]
        return ac
      }, {})
  }

  useEffect(() => {
    let token = ''
    let code = ''
    const location = document.location

    // using query
    const query = new URLSearchParams(location.search)
    token = (query && query.get('token')) || ''
    code = (query && query.get('code')) || ''

    // using header
    const req = new XMLHttpRequest()
    req.open('GET', location.origin, false)
    req.send(null)
    const headers = parseHttpHeaders(req.getAllResponseHeaders())

    Object.keys(headers).map((el) => {
      if (el === 'token' && headers[el] !== '') token = headers[el]
      if (el === 'code' && headers[el] !== '') code = headers[el]
    })

    setToken(token)
    setParams({ ...params, code: code })
  }, [])

  useEffect(() => {
    const res = chart.filter((el: any, index: number) => {
      if (index === 0 || (index > 0 && el.time !== chart[index - 1].time))
        return el
    })
    setChartData(res)
  }, [chart])

  const setRange = (payload: string) => {
    setParams({ ...params, range: payload })
  }

  const fetchData = async () => {
    setLoading(true)
    await fetchChart({
      params: params,
      token: token,
    })
    setLoading(false)
  }

  useEffect(() => {
    params.code !== '' && fetchData()
  }, [params])

  return (
    <div className="w-screen h-screen">
      <div className="h-full flex justify-center items-center">
        {loading && (
          <div className="w-1/2 bg-transparent h-full">
            <Image
              className="animate-pulse"
              src="/images/logo.svg"
              alt="emtrade"
              layout="responsive"
              width={'100%'}
              height={'100%'}
            />
          </div>
        )}
        {!loading && chartData.length > 0 && <ChartCandle data={chartData} />}
      </div>
      <div className="flex justify-between w-screen sticky bottom-0 z-10 bg-white border-t-2 border-gray-100">
        <div className="flex p-3 w-full justify-between items-center text-sm text-gray-600 font-semibold">
          {rangeList.map((res, index) => {
            return (
              <div
                key={index}
                className={`cursor-pointer p-3 hover:text-teal-600 ${
                  params && params.range === res && 'text-teal-600 '
                }`}
                onClick={() => setRange(res)}
              >
                {res}
              </div>
            )
          })}
          <a href="https://member.emtrade.id/chart">
            <Image
              className="cursor-pointer"
              src="/images/chart.png"
              alt="me"
              width="30"
              height="30"
            />
          </a>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state: any) => ({
  chart: state.chart && state.chart.data,
})

const mapDispatchToProps = (dispatch: any) => ({
  fetchChart: bindActionCreators(fetchChart, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(Index)
