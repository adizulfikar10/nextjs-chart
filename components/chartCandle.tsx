import { createChart } from 'lightweight-charts'
import { useEffect, useRef } from 'react'

interface IProps {
  data: any
}

const ChartCandle = (props: IProps) => {
  const chartRef = useRef(null)

  const { data } = props

  let chart: any = null

  useEffect(() => {
    chart = createChart(chartRef.current, {
      timeScale: {
        rightOffset: data.length > 10 ? 3 : 15,
        barSpacing: screen.width / 20,
        lockVisibleTimeRangeOnResize: true,
        rightBarStaysOnScroll: true,
      },
      grid: {
        vertLines: {
          color: 'rgba(197, 203, 206, 0.0)',
        },
        horzLines: {
          color: 'rgba(197, 203, 206, 0.0)',
        },
      },
    })

    const candleSeries = chart.addCandlestickSeries()
    candleSeries.setData(data)
  }, [])

  return <div className="w-full h-full" ref={chartRef}></div>
}

export default ChartCandle
